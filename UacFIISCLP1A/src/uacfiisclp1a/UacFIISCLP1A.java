/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uacfiisclp1a;

import java.util.Scanner;


public class UacFIISCLP1A {
    
    /**
    * Clase      : imprimirMensaje
    * Parámetros : String sMensaje
    * Acción     : Imprime el mensaje enviado por parametro y salta una línea
    * Upadate    : 12.05.2020
    */    
    static void imprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    }
    
    static void separador(){
        imprimirMensaje("-----------------------------------------");
    }
    
    static void Encabezado(){
        imprimirMensaje("Universidad Autónoma de Campeche");
        imprimirMensaje("Facultade de Ingeniería");
        imprimirMensaje("Ingeniería en Sistemas Computacionales");
        imprimirMensaje("Lenguajes de Programación 1 A");
        imprimirMensaje("EDCD");
        separador();
    }

    static void piepagina(){
        separador();
        imprimirMensaje("(c) UACAM-FI-ISC-LP1-A");
    }
 
    static void comparavalores(){
        
        Scanner entrada = new Scanner( System.in );
        imprimirMensaje("Leer Valor1 : ");        
        int iValor1 = entrada.nextInt();
        imprimirMensaje("Leer Valor2 : ");        
        int iValor2 = entrada.nextInt();
        if (iValor1 > iValor2){
            imprimirMensaje("Valor1 > Valor2 : ");     
        } else {
            if (iValor1 == iValor2){
               imprimirMensaje("Valor1 y Valor2 son iguales ");                     
                
            } else {
               imprimirMensaje("Valor2 > Valor1 : ");                     
            }    
        }
    }
    /*
    *
    */
    static void triangulo(){
        Scanner entrada = new Scanner( System.in );
        imprimirMensaje("Leer Base : ");        
        int iBase = entrada.nextInt();
        imprimirMensaje("Leer Altura : ");        
        int iAltura = entrada.nextInt();
        double dArea = (iBase * iAltura)/2;
        imprimirMensaje("El Área del Triangulo es : "+ dArea);                     
    }
   

    static void submenu(int opcion){
        if (opcion == 1){
            imprimirMensaje("Comparar dos números");
            separador();
            comparavalores();
        }
        if (opcion == 2){
            imprimirMensaje("Triangulo");
            separador();
            triangulo();
        }
        if (opcion == 3){
            imprimirMensaje("Cuadrado");
            separador();
        }
        if (opcion == 4){
            imprimirMensaje("Circulo");
            separador();
        }
        if (opcion == 5){
            imprimirMensaje("Pentagono");
            separador();
        }
        if (opcion == 9){
            imprimirMensaje("Gracias por utilizar nuestros aplicativos");
             separador();
       }
    }
    
    static void menu(){
        imprimirMensaje("1.- Comparar dos Números ");
        imprimirMensaje("2.- Calcular el área de un Triangulo ");
        imprimirMensaje("3.- Calcular el área de un Cuadrado");
        imprimirMensaje("4.- Calcular el área de un Circulo");
        imprimirMensaje("5.- Calcular el área del Pentagono");
        imprimirMensaje("9.- Salir");
        Scanner entrada = new Scanner( System.in );
        imprimirMensaje("Teccle la opción deseada __ ");        
        int iOpcion = entrada.nextInt();
        submenu(iOpcion);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Encabezado();        
        menu();
        piepagina();                
    }          
}
