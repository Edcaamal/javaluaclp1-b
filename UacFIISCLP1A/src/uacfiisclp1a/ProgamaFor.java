/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uacfiisclp1a;

/**
 *
 * @author edgar
 */
public class ProgamaFor {

    static void ejemplosForArray(){
       int[] numeros = {3,2,1,1,2,4,5,6,7,8,5,7,2,4,10};
        for (int dato : numeros) {
            System.out.println("valor: " + dato);
        }
        String[] diasSemana = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
        for (String dato : diasSemana) {
            System.out.println("valor: " + dato);
        }
        
        String[] mesAnno = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio"};
        for (String dato : mesAnno) {
            System.out.println("valor: " + dato);
        }
    }
    
     static void ejemplosFor(){
         for (int i= 100; i >= 0; i-=3){
             System.out.println("valor: " + i);
         }
         for (int i= 1; i <= 10; i++){
             System.out.println("valor: " + i);
         }
         for (int i= 10; i >= 0; i--){
             System.out.println("valor: " + i);
         }
         
         
         
     }
  
    
    public static void main(String[] args) {
        // TODO code application logic here
        // ejemplosForArray();
        ejemplosFor();
        
     }
    
}
