/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalp1b;

import java.util.Scanner;

/**
 *
 * @author edgar
 */
public class Electrodomestico {
    private String marca;
    private String modelo;
    private int    anno;
    private double capacidad;

    public Electrodomestico() {
    }

    public Electrodomestico(String marca, String modelo, int anno, double capacidad) {
        this.marca = marca;
        this.modelo = modelo;
        this.anno = anno;
        this.capacidad = capacidad;
    }



    
    static void imprirobjeto (Electrodomestico myObjeto){
        /*
        System.out.println(myObjeto.marca);
        System.out.println(myObjeto.modelo);
        System.out.println(myObjeto.anno);
        */
        System.out.println(myObjeto.toString());
        System.out.println("------------------------------------");   
    }

    @Override
    public String toString() {
        return "Electrodomestico{" + "marca=" + marca + ", modelo=" + modelo + ", anno=" + anno + ", capacidad=" + capacidad + '}';
    }

    public void encendido(String horaEncendido){
        System.out.println(horaEncendido);
        
    }

   
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Scanner entrada = new Scanner(System.in);
        
        Electrodomestico refrigerador = new Electrodomestico();
        refrigerador.marca     = "Mabe";
        refrigerador.modelo    = "Con Escarcha";
        refrigerador.anno      = 2019;
        refrigerador.capacidad = 50.00;
        
        imprirobjeto(refrigerador);
                
        Electrodomestico estufa = new Electrodomestico();
        estufa.marca   = "IEM";
        estufa.modelo  = "Con Asador";
        estufa.anno    = 2020;
        estufa.encendido("8:30");
        
        
        imprirobjeto(estufa);
        
        Electrodomestico television = new Electrodomestico("SONY", "Smart", 2020, 10);
        television.encendido("20:00");
        imprirobjeto(television);

        Electrodomestico laptop = new Electrodomestico("HP", "iCore5", 2020, 2);
        imprirobjeto(television);
        
             
        
    }
    
}
