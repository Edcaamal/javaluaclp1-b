/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalp1b;

import java.util.Scanner;

/**
 *
 * @author edgar
 */
public class Estructuras_Case_For {
    
    static void forIncremental(){
        System.out.println("Escogiste la opción 1: For Incremental");
        for (int i= 1; i<=10; i++){
            System.out.println("i = "+ i);
        }
        
    }
    
    /** Método     : forDecremental()
    * Uso          : Ciclo for con decrementos de 1 en 1
    * Parámetros   : No recibe parametros
    */
    static void forDecremental(){
        System.out.println("Escogiste la opción 2: For decremental");
        for (int i= 10; i>=1; i--){
            System.out.println("i = "+ i);
        }      
    }

    static void forDecremental2(){
        System.out.print("Opcion 3: Decrementos de dos en dos");
        for (int i= 20; i>=1; i-= 2){
            System.out.println("i = "+ i);
        }                
    }
    
    static void forEach(){
        System.out.print("Escogiste la opción 4: For each");
        int[] arreglo = {3,2,7,4,5,9,7,8,22,4, 6, 78,0};
        for (int valor : arreglo){
            System.out.println("Valor = "+valor);
        }                
    }

    static void whileIncremental(){
        System.out.println("Escogiste la opción 5 : While");
        int j= 1;
        while (j<= 10){
            System.out.println("Valor de J = "+j);
            j++;
        }                
    }

    static void whileIncrementalBooleno(){
        Boolean condicion = true;       
        System.out.println("Escogiste la opción 7 : While Incremental Boolean");
        int j= 1;
        while (condicion){
            System.out.println("Valor de J = "+j);
            j++;
            if (j>= 10) {
                condicion = false;
            }
        }                
    }
    
    static void whileDecremental(){
        System.out.print("Escogiste la opción 6: While decremental");
        System.out.println("Escogiste la opción 5 : While");
        int k= 20;
        while (k>= 1){
            System.out.println("Valor de K = "+k);
            k--;
        }                
    }

    static void doWhileIncremental(){
        System.out.print("Escogiste la opción 6: While decremental");
        System.out.println("Escogiste la opción 5 : While");
        int k= 2;
        do {
            System.out.println("Valor de K = "+k);
            k++;
        }while (k<= 15);              
    }

    static void menuPrincipal(){
        System.out.println("==================================================");
        System.out.println("1.- For Incremental");
        System.out.println("2.- For Decremental");
        System.out.println("3.- For Decremental de 2 en 2");
        System.out.println("4.- For each");
        System.out.println("5.- While Incremetal");
        System.out.println("6.- While Decremental");
        System.out.println("7.- While Booleano");
        System.out.println("8.- do While Incremental");
        System.out.println("0.- Salir");
    }
    
    static void subMenu(int expresion){
        switch(expresion){
            case 1:
                forIncremental();
                break;
            case 2 : 
                forDecremental();             
                break;
            case 3 :
                forDecremental2();
                break;                
            case 4 :
                forEach();
                break;
            case 5 :
                whileIncremental();
                break;
            case 6 :
                whileDecremental();
                break;
            case 7 :
                whileIncrementalBooleno();
                break;
            case 8 :
                doWhileIncremental();
                break;
            case 0 :
                System.out.println("Gracias por participar");
                break;
            default:
                System.out.print("No Escogiste una opción valida");
                break;
        }
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int opcion;
        do {
           menuPrincipal();
           Scanner entrada = new Scanner(System.in);
           System.out.println("Teccele una opción : ");
           opcion  = entrada.nextInt(); 
           subMenu(opcion);
        } while (opcion != 0);
    }
    
}
